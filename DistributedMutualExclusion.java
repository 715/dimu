class DistributedMutualExclusion extends MyObject {

   public static void main(String[] args) {

      // parse command line options, if any, to override defaults
      GetOpt go = new GetOpt(args, "Un:R:");
      String usage = "Usage: -n numNodes -R runTime"
         + " napOutsideCS[i] napInsideCS[i] i=0,1,...";
      go.optErr = true;
      int ch = -1;
      int numNodes = 5;
      int runTime = 60;      // seconds
      while ((ch = go.getopt()) != go.optEOF) {
         if      ((char)ch == 'U') {
            System.out.println(usage);  System.exit(0);
         }
         else if ((char)ch == 'n')
            numNodes = go.processArg(go.optArgGet(), numNodes);
         else if ((char)ch == 'R')
            runTime = go.processArg(go.optArgGet(), runTime);
         else {
            System.err.println(usage);  System.exit(1);
         }
      }
      System.out.println("DistributedMutualExclusion: numNodes="
         + numNodes + ", runTime=" + runTime);

      // process non-option command line arguments
      int[] napOutsideCS = new int[numNodes];
      int[] napInsideCS = new int[numNodes];
      int argNum = go.optIndexGet();
      for (int i = 0; i < numNodes; i++) {
         napOutsideCS[i] = go.tryArg(argNum++, 8);
         napInsideCS[i] = go.tryArg(argNum++, 2);
      }
      // create communication channels
      MessagePassing[] requestChannel = null, replyChannel = null,
         requestChannelS = null, requestChannelR = null,
         replyChannelS = null, replyChannelR = null;
      requestChannel = new MessagePassing[numNodes];
      replyChannel = new MessagePassing[numNodes];
      requestChannelS = new MessagePassing[numNodes];
      replyChannelS = new MessagePassing[numNodes];
      requestChannelR = new MessagePassing[numNodes];
      replyChannelR = new MessagePassing[numNodes];
      for (int i = 0; i < numNodes; i++) {
         requestChannel[i] = new AsyncMessagePassing();
         replyChannel[i] = new AsyncMessagePassing();
         requestChannelS[i] = new MessagePassingSendOnly(requestChannel[i]);
         replyChannelS[i] = new MessagePassingSendOnly(replyChannel[i]);
         requestChannelR[i] = new MessagePassingReceiveOnly(requestChannel[i]);
         replyChannelR[i] = new MessagePassingReceiveOnly(replyChannel[i]);
      }

      // create the Nodes (they start their own threads)
      for (int i = 0; i < numNodes; i++)
         new Node("Node", i, numNodes,
            napOutsideCS[i]*1000, napInsideCS[i]*1000,
            requestChannelS, replyChannelS,
            requestChannelR[i], replyChannelR[i]);
      System.out.println("All Nodes created");

      // let the Nodes run for a while
      nap(runTime*1000);
      System.out.println("age()=" + age()
         + ", time to stop the threads and exit");
      System.exit(0);
   }
}
